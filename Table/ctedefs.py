# -*- coding: utf-8 -*-
"""
Created on Thu May 16 

@author: Lynn 
"""

from cte import CTE
active_empl_cte = CTE ("COUNT_EMPL", [], '''
 select 
    to_char(date 'CURRENT_DATE', 'YYYY-MM-DD') as monthDate,
    EMPL_CLASS as emplClass,
    count(distinct EMPLID) as numOfEmpl 
    from (
        select 
            EMPLID,
            EMPL_RCD,
            FTE, 
            DEPTID,
            EMPL_CLASS,
            RANK() over (partition by EMPLID, EMPL_RCD order by EFFDT desc, EFFSEQ desc, COMP_EFFSEQ desc) as actionRank
            from PS_UW_HR_ALLJOB_VW 
            where
            UW_JOB_START_DATE <= date 'CURRENT_DATE' and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= date 'CURRENT_DATE')
            and 
            BUSINESS_UNIT = 'UWMSN'
    )
    where actionRank = 1 
    group by EMPL_CLASS
    order by EMPL_CLASS
''')

active_job_cte = CTE ("COUNT_EMPL", [], '''
 select 
    to_char(date 'CURRENT_DATE', 'YYYY-MM-DD') as monthDate,
    count(*) as numOfJobs,
    substr(DEPTID, 1, 3) as division
    from (
        select 
            EMPLID,
            EMPL_RCD,
            FTE, 
            DEPTID,
            EMPL_CLASS,
            RANK() over (partition by EMPLID, EMPL_RCD order by EFFDT desc, EFFSEQ desc, COMP_EFFSEQ desc) as actionRank
            from PS_UW_HR_ALLJOB_VW 
            where
            UW_JOB_START_DATE <= date 'CURRENT_DATE' and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= date 'CURRENT_DATE')
            and 
            BUSINESS_UNIT = 'UWMSN'
    )
    where actionRank = 1 
    group by substr(DEPTID, 1, 3)
    order by substr(DEPTID, 1, 3)
''')

