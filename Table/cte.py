# -*- coding: utf-8 -*-

class CTE:
    def __init__(self, name, depends, sql):
        self.name = name
        self.sql = sql 
        self.depends = depends 

    # prepend the dependencies 
    def expand_dependencies(self):
        s = ""
        for d in self.depends:
            s += d.expand_dependencies()   # recursion 
            s += ', \n'
        s += self.name + " as (\n"
        s += self.sql + "\n)"
        return s 

    def query_full(self):
        s = "with\n"
        s += self.expand_dependencies()
        s += "\n"
        s += "select * from " + self.name + "\n"
        return s