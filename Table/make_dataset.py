# Makes the dataset. Contains the main() function
import click 
import logging # log file 
import os 
import sys 
import importlib
import os.path
import cte 
import ctedefs
import pandas as pd 
import datetime as dt 
from pandas import ExcelWriter
from pathlib import Path 
from dotenv import find_dotenv, load_dotenv 
from sqlalchemy import create_engine 

@click.command()
@click.argument('output_filepath', type = click.Path(), required = False)
def main(output_filepath):
    ''' Run data processing scripts to turn raw data from (.../raw) into cleaned data

    ready to be analyzed (saved in the current directory).
    '''
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    logger.info('Project Dir = ' + str(project_dir))  
    logger.info('Userprofile = ' + str(userprofile_dir))

    sys.path.append(str(userprofile_dir)) # find module 
    connection = importlib.import_module(connection_module) # dynamically import connection.py as module 
    sys.path.remove(str(userprofile_dir))

    engine = create_engine(connection.epm()) # epm() function in connection module, create a connection string
    con = engine.connect() 

    # random testing 1 
    df = pd.read_sql('select 1 as cnt from dual', con)
    if (df['cnt'][0] == 1):
        result = 'success'
    else:
        result = 'failure'
    # Logger.info(msg): this will log a message with level INFO on this logger 
    logger.info('EPM connection test: ' + result)

    # iterate through the past 12 months 
    cumulativeDf = pd.DataFrame() 
    cumulativeDf2 = pd.DataFrame()
    dateList = ['2019-05-01', '2019-04-01', '2019-03-01', '2019-02-01', '2019-01-01', '2018-12-01', '2018-11-01',
    '2018-10-01', '2018-09-01', '2018-08-01', '2018-07-01', '2018-06-01']
    
    if (output_filepath is None or output_filepath == ''):
            export_xlsx_filename = 'EMPLOYEES_' + timestamp + '.xlsx'
            export_xlsx_pathname = project_dir / export_xlsx_filename
    else:
            export_xlsx_pathname = output_filepath      
    logger.info('XLSX path: ' + str(export_xlsx_pathname))
    writer = pd.ExcelWriter(export_xlsx_pathname, engine='xlsxwriter')

    for date in dateList:
        numEmplDf = pd.read_sql(ctedefs.active_empl_cte.query_full().replace("CURRENT_DATE", date), con)
        numOfJobs = pd.read_sql(ctedefs.active_job_cte.query_full().replace("CURRENT_DATE", date), con)
        cumulativeDf = cumulativeDf.append(numEmplDf)
        cumulativeDf2 = cumulativeDf2.append(numOfJobs)

    # output the result to an excel 
    cumulativeDf.pivot_table(index='emplclass', columns='monthdate', values='numofempl').to_excel(writer,
                       sheet_name = 'Sheet1')
       
    cumulativeDf2.pivot_table(index='division', columns='monthdate', values='numofjobs').to_excel(writer,
                       sheet_name = 'Sheet2')
    writer.save()
       

if __name__ == '__main__':
    project_dir = Path(__file__).resolve().parents[0]  
    timestamp = dt.datetime.now().strftime("%Y%m%d%H%M%S")

    # path to log file 
    logpath = project_dir / 'reports' / ('make_dataset_' + timestamp + '.log')
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level = logging.DEBUG, format = log_fmt, filename = logpath)

    userprofile_dir = Path(os.environ['USERPROFILE']) # environment var 
    connection_module = 'connection' # connection.py in the home dir 

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()