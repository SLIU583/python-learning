import base64

def mksid(user,pswd,dsn,api):
    s = 'oracle://{USER}:{PSWD}@{DSN}'.format(USER=user,PSWD=pswd,DSN=dsn)
    return(s)

def epm(user='',pswd='',api='sqlalchemy'):
    return(mksid(user,base64.b64decode(pswd).decode(),'EPM',api))

def infoaccess(user='',pswd='',api='sqlalchemy'):
    return(mksid(user,base64.b64decode(pswd).decode(),'INFOACCESS',api))

def wisdm(user='',pswd='',api='sqlalchemy'):
    return(mksid(user,base64.b64decode(pswd).decode(),'WISDM',api))

def ohrcommon(user='',pswd='',api='sqlalchemy'):
    return(mksid(user,base64.b64decode(pswd).decode(),'OHRCOMMON',api))

