# Python Learning

Code and exercises to learn Python and SQL 

### make_dataset.py
* Some of the packages needed are illustrated 
* How to connect to Oracle with Pandas and SQL Alchemy


### cte.py
* helper functions for working with Oracle CTE blocks 
* Note: TODO - Generalize cte.py into more comprehensive package 


### ctedefs.py 
* A bunch of Oracle CTE code wrapped into Python functiosn 


### connection_sample.py 
* Example of how to encode Oracle connection information and store it on disk 


## Task 2
1. For each employee class, calculate the # of employees for each month over 
the past year and output it to an excel file using python (could reuse 
the sql written before)

2. For each division (parse as a substring), calculate the # of employees 
for each month over the past year and output it to a seperate excel file 