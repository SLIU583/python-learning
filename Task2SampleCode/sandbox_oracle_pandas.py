# -*- coding: utf-8 -*-
"""
Sandbox code for testing different ideas with Oracle and Pandas

Jon Vander Hill
05/22/2019
"""
# Python Standard Libraries
from pathlib import Path # directory path manipulation
import sys # System-specific parameters and functions
import importlib # Implementation of python "import" statement
import datetime as dt # Date/Time functions
import os

# Python Data Science Libraries
import pandas as pd # Pandas dataframe manipulation 
from sqlalchemy import create_engine # RDBMS integration w/ Pandas

# Dynamic loading of EPM credentials
# Expects $USERPROFILE/connection.py exists and contains a function epm()
# that will return a properly formed Oracle connection string

# Pull value of USERPROFILE form environment variables
userprofile_dir = Path(os.environ['USERPROFILE'])  
# Dynamically load $USERPROFILE/connection.py
connection_module = 'connection'
sys.path.append(str(userprofile_dir))
connection = importlib.import_module(connection_module)
sys.path.remove(str(userprofile_dir))

# Connect to EPM
# Note the call to connection.epm() to build connection string
engine = create_engine(connection.epm())
con = engine.connect()

# Run a test query - experiment with try/except around database calls
try:
  df_test = pd.read_sql('select 1 as cnt from dual', con)
except:
  print('EPM Connection Test exception')
  raise # reraise the exception - this will stop execution with a stack trace
else:
  if (df_test['cnt'][0] == 1):
    result = 'success'
  else:
    result = 'failure'
  print('EPM Connection Test: ' + result)


# Pythonic current date
current_date = dt.datetime.now()
# Convert to ANSI SQL format
current_date_ansi = current_date.strftime("DATE '%Y-%m-%d'")
# Convert to Oracle to_date() format
current_date_oracle = current_date.strftime("to_date('%Y-%m-%d', 'YYYY-MM-DD')")

print(current_date)

# CTE to pull most recent job action from PS_UW_HR_ALLJOB_VW
action_current_cte = '''
ACTION_CURRENT as (
  select A.*
  from (
    select EMPLID
          ,EMPL_RCD
          ,UW_JOB_START_DATE
          ,UW_JOB_END_DT
          ,EMPL_CLASS
          ,DEPTID
          ,rank() over (partition by EMPLID
                                    ,EMPL_RCD 
                        order by     EFFDT desc
                                    ,EFFSEQ desc
                                    ,COMP_EFFSEQ desc) as ACTION_RANK
    from SYSADM.PS_UW_HR_ALLJOB_VW
    where EFFDT <= {AS_OF_DATE} /* as of job actions */
      and  UW_JOB_START_DATE <= {AS_OF_DATE}  /* as of jobs */
      and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= {AS_OF_DATE})
  ) A
  where A.ACTION_RANK = 1
)
'''

# Some common parameters
as_of_date=current_date_ansi
where_clause="where DEPTID like 'A%'"

# Main query to count EMPLID's
job_count_query = '''
with

{ACTION_CURRENT_CTE}

select count(EMPLID) as CNT
      ,count(distinct EMPLID) as DCNT
      ,sum(case when EMPLID is NULL then 1 else 0 end) as NCNT
from ACTION_CURRENT
{WHERE_CLAUSE}
'''
action_current_cte_as_of = action_current_cte.format(AS_OF_DATE=as_of_date)
query = job_count_query.format(ACTION_CURRENT_CTE=action_current_cte_as_of,
                               WHERE_CLAUSE=where_clause)

#print(query)
df_cnt = pd.read_sql(query, con)
if (df_cnt['cnt'][0] > 0):
    result = 'success'
else:
    result = 'failure'
print('EPM Job Count Test: ' + result)

print(df_cnt)


# Main query that will pull job records
job_record_query = '''
with

{ACTION_CURRENT_CTE}

select *
from ACTION_CURRENT
{WHERE_CLAUSE}
'''
action_current_cte_as_of = action_current_cte.format(AS_OF_DATE=as_of_date)
query = job_record_query.format(ACTION_CURRENT_CTE=action_current_cte_as_of,
                               WHERE_CLAUSE=where_clause)

#print(query)
df_jobs = pd.read_sql(query, con)

# This section builds a data from of grouped counts by EMPL_CLASS

# Count non-null EMPLID by EMPL_CLASS (i.e. number of jobs per EMPL_CLASS)
# returns a Pandas Series object
empl_class_cnt = df_jobs.groupby('empl_class').emplid.count()

# Count unique EMPLID by EMPL_CLASS
# returns a Pandas Series object
empl_class_dcnt = df_jobs.groupby('empl_class').emplid.nunique()

# Concatenate counts into a new dataframe
empl_class_df = pd.concat([empl_class_cnt, empl_class_dcnt], axis=1)
empl_class_df.columns = ['cnt','dcnt']
empl_class_df = empl_class_df.reset_index()

# This section builds a data from of grouped counts by DIVISION

# Add a DIVISION column to the dataframe pulled from from EPM
df_jobs['division'] = df_jobs.apply(lambda row: row['deptid'][0:3], axis=1)

# Count non-null EMPLID by EMPL_CLASS (i.e. number of jobs per EMPL_CLASS)
# returns a Pandas Series object
division_cnt = df_jobs.groupby('division').emplid.count()

# Count unique EMPLID by EMPL_CLASS
# returns a Pandas Series object
division_dcnt = df_jobs.groupby('division').emplid.nunique()

# Concatenate counts into a new dataframe
division_df = pd.concat([division_cnt, division_dcnt], axis=1)
division_df.columns = ['cnt','dcnt']
division_df = division_df.reset_index()