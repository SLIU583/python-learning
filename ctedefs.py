# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 13:59:09 2019

@author: jvanderhill
"""
from cte import CTE

empl_class_rank_cte = CTE('EMPL_CLASS_RANK', [], '''
        select 'LI' as EMPL_CLASS, 1 as RANK from dual
  union select 'FA' as EMPL_CLASS, 2 as RANK from dual
  union select 'AS' as EMPL_CLASS, 3 as RANK from dual
  union select 'CP' as EMPL_CLASS, 4 as RANK from dual
  union select 'CJ' as EMPL_CLASS, 5 as RANK from dual
  union select 'CL' as EMPL_CLASS, 6 as RANK from dual
  union select 'OT1' as EMPL_CLASS, 7 as RANK from dual
  union select 'ET1' as EMPL_CLASS, 8 as RANK from dual
  union select 'ET2' as EMPL_CLASS, 9 as RANK from dual
  union select 'ET3' as EMPL_CLASS, 10 as RANK from dual
  union select 'ET4' as EMPL_CLASS, 11 as RANK from dual
  union select 'SA1' as EMPL_CLASS, 12 as RANK from dual
  union select 'SA2' as EMPL_CLASS, 13 as RANK from dual
  union select 'SA3' as EMPL_CLASS, 14 as RANK from dual
  union select 'SA4' as EMPL_CLASS, 15 as RANK from dual
  union select 'SA5' as EMPL_CLASS, 16 as RANK from dual
  union select 'SA6' as EMPL_CLASS, 17 as RANK from dual
  union select 'SA7' as EMPL_CLASS, 18 as RANK from dual
  union select 'SH' as EMPL_CLASS, 19 as RANK from dual
  union select 'OT2' as EMPL_CLASS, 20 as RANK from dual
  union select 'OT3' as EMPL_CLASS, 21 as RANK from dual
  union select 'OT4' as EMPL_CLASS, 22 as RANK from dual
  union select 'OT5' as EMPL_CLASS, 23 as RANK from dual
  union select 'OT6' as EMPL_CLASS, 24 as RANK from dual
''')

pay_basis_rank_cte = CTE('PAY_BASIS_RANK', [], '''
        select 'A' as PAY_BASIS, 1 as RANK from dual
  union select 'C' as PAY_BASIS, 2 as RANK from dual
  union select 'H' as PAY_BASIS, 3 as RANK from dual
  union select 'L' as PAY_BASIS, 4 as RANK from dual
  union select 'S' as PAY_BASIS, 5 as RANK from dual
  union select 'V' as PAY_BASIS, 6 as RANK from dual
  union select 'N' as PAY_BASIS, 7 as RANK from dual
''')

empl_status_rank_cte = CTE('EMPL_STATUS_RANK', [], '''
        select 'A' as EMPL_STATUS, 1 as RANK from dual
  union select 'P' as EMPL_STATUS, 2 as RANK from dual
  union select 'L' as EMPL_STATUS, 3 as RANK from dual
  union select 'T' as EMPL_STATUS, 4 as RANK from dual
''')

action_prep_cte = CTE('ACTION_PREP',[],'''
    select EMPLID
          ,EMPL_RCD
          ,UW_JOB_START_DATE
          ,(case when UW_JOB_END_DT is null then DATE '2099-12-31' else UW_JOB_END_DT end) as UW_JOB_END_DT
          ,EFFDT       
          ,EFFSEQ
          ,COMP_EFFSEQ 

    from SYSADM.PS_UW_HR_ALLJOB_VW
    where EFFDT <= to_date(CURRENT_DATE-1) /* don't want future dated job actions */
      and  UW_JOB_START_DATE <= to_date(CURRENT_DATE-1)  /* or jobs */
      and (UW_JOB_END_DT is NULL 
           or UW_JOB_END_DT >= to_date(CURRENT_DATE-1))
''')

action_current_cte = CTE('ACTION_CURRENT',[action_prep_cte],'''
  select A.*
  from (
    select AP.* 

          ,rank() over (partition by AP.EMPLID, AP.EMPL_RCD, AP.UW_JOB_START_DATE, AP.UW_JOB_END_DT 
                        order by AP.EFFDT desc, AP.EFFSEQ desc, AP.COMP_EFFSEQ desc) as ACTION_RANK

    from ACTION_PREP AP
  ) A
  where ACTION_RANK=1
''')


#select count(EMPLID), count(distinct EMPLID) from ACTION_PREP
# 881,243 80,637 

#JOB as (
#  select A.*
#  from ACTION_CURRENT A
#  where A.ACTION_RANK = 1
#),
#
#/*select count(EMPLID), count(distinct EMPLID) from JOB*/
#/* 98,962 80,637 */
#
curjob_cte = CTE('CURJOB',[action_current_cte],
'''
  select ACTION.EMPLID
        ,ACTION.EMPL_RCD
        ,ACTION.UW_JOB_START_DATE
        ,ACTION.UW_JOB_END_DT
        ,ALLJOB.POSITION_NBR
        ,ALLJOB.NAME
        ,substr(ALLJOB.DEPTID,1,3) as UDDS
        ,ALLJOB.DEPTID
        ,ALLJOB.UW_DEPTID_DESCR
        ,ALLJOB.EMPL_STATUS
        ,ALLJOB.FTE
        ,ALLJOB.EMPL_CLASS
        ,ALLJOB.UW_PAY_BASIS

  from ACTION_CURRENT ACTION
  left join SYSADM.PS_UW_HR_ALLJOB_VW ALLJOB
  on (ACTION.EMPLID = ALLJOB.EMPLID
      and ACTION.EMPL_RCD = ALLJOB.EMPL_RCD
      and ACTION.EFFDT = ALLJOB.EFFDT
      and ACTION.EFFSEQ = ALLJOB.EFFSEQ
      and ACTION.COMP_EFFSEQ = ALLJOB.COMP_EFFSEQ)

  where ALLJOB.DEPTID like 'A%'
    and ALLJOB.EMPL_CLASS in ('FA','AS','LI','CP','CJ','CL')
    and ALLJOB.UW_PAY_BASIS != 'N'
''')
#
#/*select count(EMPLID), count(distinct EMPLID) from CURJOB*/
#/* 18,043 17,575 */
#
#
primjob_prep_cte = CTE('PRIMJOB_PREP',
                       [empl_status_rank_cte,
                        empl_class_rank_cte,
                        pay_basis_rank_cte,
                        curjob_cte],
'''
  select
         CURJOB.*
        
        ,(case when PRMJOB.BENEFIT_RCD_NBR is NULL then 0
          else PRMJOB.BENEFIT_RCD_NBR
          end)                            as BENEFIT_RCD_NBR
        ,(case when PRMJOB.PRIMARY_JOB_IND is NULL then 'N' 
          else PRMJOB.PRIMARY_JOB_IND 
          end)                            as PRIMARY_JOB_IND
 
        ,ESR.RANK                         as EMPL_STATUS_RANK
        ,ECR.RANK                         as EMPL_CLASS_RANK
        ,PBR.RANK                         as PAY_BASIS_RANK

  from CURJOB

  left join SYSADM.PS_UW_BN_PRMJOB_VW PRMJOB
  on (CURJOB.EMPLID = PRMJOB.EMPLID
      and CURJOB.EMPL_RCD = PRMJOB.EMPL_RCD)
  
  left join EMPL_STATUS_RANK ESR
  on (CURJOB.EMPL_STATUS = ESR.EMPL_STATUS)
  
  left join EMPL_CLASS_RANK ECR
  on (CURJOB.EMPL_CLASS = ECR.EMPL_CLASS)

  left join PAY_BASIS_RANK PBR
  on (CURJOB.UW_PAY_BASIS = PBR.PAY_BASIS)
''')
#
#/*select count(EMPLID), count(distinct EMPLID) from PRIMJOB_PREP*/
#/* 18,043 17,575 */
#
primjob_cte = CTE('PRIMJOB',[primjob_prep_cte],
'''
  select P.*
        ,ADRBUS.UW_BN_EMAIL_ADDR 
        ,CURRENT_DATE-1 as OHR_AS_OF_DT
        ,CURRENT_DATE as OHR_EXTRACT_DTTM
  from (
    select PRIMJOB_PREP.*
          ,rank() over( partition by PRIMJOB_PREP.EMPLID 
                        order by PRIMJOB_PREP.BENEFIT_RCD_NBR desc, 
                                 PRIMJOB_PREP.PRIMARY_JOB_IND desc, 
                                 PRIMJOB_PREP.EMPL_STATUS_RANK, 
                                 PRIMJOB_PREP.FTE desc, 
                                 PRIMJOB_PREP.EMPL_CLASS_RANK, 
                                 PRIMJOB_PREP.PAY_BASIS_RANK,
                                 PRIMJOB_PREP.UW_JOB_START_DATE,
                                 PRIMJOB_PREP.EMPL_RCD)    as PRIMJOB_RANK
    from PRIMJOB_PREP
  ) P
  left join SYSADM.PS_UW_HR_ADRBUS_VW ADRBUS
  on (P.EMPLID = ADRBUS.EMPLID)
  where P.PRIMJOB_RANK=1
''')
