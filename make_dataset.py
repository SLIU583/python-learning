# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import os
import sys
import importlib
import pandas as pd
from sqlalchemy import create_engine
import os.path
import datetime as dt

import cte
import ctedefs

@click.command()
#@click.argument('input_filepath', type=click.Path(exists=True))
#@click.option('--epmuser',type=click.STRING,required=True)
#@click.option('--epmpswd',type=click.STRING,required=True)
@click.argument('output_filepath', type=click.Path(),required=False)
def main(output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    logger.info('Project Dir=' + str(project_dir))
    logger.info('Userprofile=' + str(userprofile_dir))
    #click.echo(connection_module)
    sys.path.append(str(userprofile_dir))
    #click.echo(sys.path)
    connection = importlib.import_module(connection_module)
    sys.path.remove(str(userprofile_dir))
    
    engine = create_engine(connection.epm())
    con = engine.connect()
    
    df = pd.read_sql('select 1 as cnt from dual', con)
    if (df['cnt'][0] == 1):
        result = 'success'
    else:
        result = 'failure'
    logger.info('EPM Connection Test: ' + result)
    
    df = pd.read_sql(ctedefs.primjob_cte.query_colcnt('EMPLID'), con)
    if (df['emplid_cnt'][0] == df['emplid_dcnt'][0]):
        result = 'success'
    else:
        result = 'failure'
    logger.info('EMPLID check: ' + result)
    logger.info('EMPLID count: '+ str(df['emplid_cnt'][0]))
    
    primjob_df = pd.read_sql(ctedefs.primjob_cte.query_full(), con)
    
    primjob_df['ohr_as_of_dt'] = primjob_df['ohr_as_of_dt'].dt.date
    
    export_df = primjob_df[['name',
                            'udds',
                            'deptid',
                            'uw_deptid_descr',
                            'emplid',
                            'empl_class',
                            'uw_bn_email_addr',
                            'ohr_as_of_dt',
                            'ohr_extract_dttm']]
    
    export_df.columns = ['Name',
                         'UDDS',
                         'Dept Id',
                         'Dept Id Desc',
                         'Empl Id',
                         'Empl Class',
                         'Uw Bn Email Addr',
                         'OHR_AS_OF_DT',
                         'OHR_EXTRACT_DTTM']
    
 
    
    export_df=export_df.sort_values('Name')
    

    
#    export_csv_filename = 'EMPLOYEES_' + timestamp  + '.csv'
#    export_csv_pathname = project_dir / 'data/processed' / export_csv_filename
#    logger.info('CSV path: ' + str(export_csv_pathname))
    
    if (output_filepath is None or output_filepath == ''):
        export_xlsx_filename = 'EMPLOYEES_' + timestamp + '.xlsx'
        export_xlsx_pathname = project_dir / 'data/processed' / export_xlsx_filename
    else:
        export_xlsx_pathname = output_filepath
         
    logger.info('XLSX path: ' + str(export_xlsx_pathname))
    
#    export_df.to_csv(export_csv_pathname)
    
    export_df.to_excel(export_xlsx_pathname,
                       index=False,
                       sheet_name='Sheet 1')


if __name__ == '__main__':
    project_dir = Path(__file__).resolve().parents[2]
    timestamp = dt.datetime.now().strftime("%Y%m%d%H%M%S")
    
    logpath = project_dir / 'reports' / ('make_dataset_' + timestamp +'.log')
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=log_fmt, filename=logpath)
    
    userprofile_dir = Path(os.environ['USERPROFILE'])
    connection_module = 'connection'

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
