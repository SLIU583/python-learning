# -*- coding: utf-8 -*-

class CTE:
    def __init__(self, name, depends, sql):
        self.name = name
        self.sql = sql
        self.depends = depends
        
    def runnable(self):
        s = ""
        for d in self.depends:
            s += d.runnable()
            s += ",\n"
        s += self.name + " as (\n"
        s += self.sql + "\n)"
        return s
    
    def query_full(self):
        s = "with\n"
        s += self.runnable()
        s += "\n"
        s += "select * from " + self.name + "\n"
        return s
    
    def query_rowcnt(self):
        s = "with\n"
        s += self.runnable()
        s += "\n"
        s += "select count(*) as ROWCNT from " + self.name + "\n"
        return s
    
    def query_colcnt(self,colname):
        s = "with\n"
        s += self.runnable()
        s += "\n"
        s += "select count(" + colname + ") as " + colname + "_CNT\n"
        s += ",count(distinct " + colname + ") as " + colname + "_DCNT\n"
        s += "from " + self.name + "\n"
        return s
    
    
    
#test_cte=CTE('TEST','select 1 from dual',[])
#print(test_cte.runnable())
#print(test_cte.query())
#print("---")
#test2_cte=CTE('TEST2','select * from TEST',[test_cte])
#print(test2_cte.runnable())
#print(test2_cte.query())